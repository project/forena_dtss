<?php

/**
 * @file FrxDTJSONDoc.inc
 * Datatables JSON Document export.
 * @author jamesgutholm
 *
 */
class FrxDTJSONDoc extends FrxDocument {

  public function __construct() {
    $this->content_type = 'application/json';
  }


  public function render($r, $format, $options = []) {

    $dom = new DOMDocument();
    $dom->strictErrorChecking = FALSE;

    libxml_use_internal_errors(TRUE);
    @$dom->loadHTML('<?xml version="1.0" encoding="UTF-8"?>' . $r->html);
    libxml_clear_errors();

    $xml = simplexml_import_dom($dom);

    $data = new stdClass();
    if ($xml === NULL) {
      watchdog(WATCHDOG_ERROR, "Null DOM found in FrxDTJSONDoc. It's likely that the query returned no data.");
      $data->{"draw"} = (string) ($forena_dtss_draw = &drupal_static('forena_dtss_draw'));
      $data->{"recordsTotal"} = '1';
      $data->{"recordsFiltered"} = '1';
      $data->{"data"} = [];
      $dataj = json_encode($data);

      $r->html .= $dataj;
      return $dataj;
    }

    // Strips the data out of the html and puts
    // it into an array to be json encoded.
    $dataRows = $this->getHtmlData($xml, '//table[@datatable=\'data\']');
    $data->{"data"} = $dataRows;

    // Strips the html out of the additional tables
    // that are providing the select list items.
    $select_data = $xml->xpath('//table[@datatable-column]');
    foreach ($select_data as $column) {
      $column_index = (string) $column['datatable-column'];
      $col_data = [];
      foreach ($column as $item) {
        $col_data[] = (string) $item->td;
      }
      $data->{"yadcf_data_" . $column_index} = $col_data;
    }

    // Get the total of all possible rows from the frx.
    $totalXml = $xml->xpath('//div[@id=\'count-1\']');
    if (!empty($totalXml) && isset($totalXml[0])) {
      $recordsTotal = trim(strip_tags($totalXml[0]->asXML()));
    }
    else {
      $recordsTotal = '-1';
    }
    $data->{"recordsTotal"} = $recordsTotal;
    $data->{"recordsFiltered"} = $recordsTotal;

    // Datatables expects the same draw number that it passes in the url
    // so we store it in the hook params alter in the module file, then
    // pass it back in the data returned to datatables.
    $data->{"draw"} = (string) ($forena_dtss_draw = &drupal_static('forena_dtss_draw'));

    return json_encode($data);
  }


  public function output(&$output) {
    $output = $this->convertCharset($output);
    parent::output($output);
    return TRUE;
  }

  /**
   * @param $xml
   *
   * @return array
   */
  private function getHtmlData($xml, $table) {
    /*
         * Generate the header data.
         *
         * Look first in the returned data for th elements.
         */
    $headerData = [];
    $hasHeader = FALSE;
    $columnCount = 0;

    $headerRowColumns = $xml->xpath($table . '//thead[1]//tr[1]//th');

    if ($headerRowColumns !== NULL && $headerRowColumns !== []) {

      $columnCount = count($headerRowColumns);
      foreach ($headerRowColumns as $column) {
        $attributes = [];
        foreach ($column->attributes() as $key => $attribute) {
          $attributes[(string) $key] = (string) $attribute;
        }
        $headerData[strip_tags($column->asXML())] = $attributes;
      }
      $hasHeader = TRUE;
    }
    else {
      /**
       * There's no header row so the number of columns in the first row is
       * examined to get the count and create the column names as "column_n".
       * There are no attributes so an empty array is provided for consistency.
       */

      $headerRowColumns = $xml->xpath($table . '//tr[1]//td');

      if ($headerRowColumns !== NULL && $headerRowColumns !== []) {
        $columnCount = count($headerRowColumns);
        for ($i = 0; $i < $columnCount; $i++) {
          $headerData['column_' . ($i + 1)] = [];
        }
      }
    }


    /**
     * At this point columnCount needs to be an integer greater than zero,
     * otherwise either no table was found or it was a table with no rows or no
     * columns.
     */

    /**
     * There are two ways that the data can be returned, either as an array of
     * arrays or as an array of objects. The array of objects will include the
     * column names for every row so it's probably not as efficient so array of
     * arrays is being used.
     */

    $dataRows = [];
    $rows = [];
    $rows = $xml->xpath($table . '//tr');

    if ($rows !== NULL && $rows !== []) {
      $i = 0;

      /**
       * The data processing does not try to account for either rowspan or
       * colspan and assumes that a value will be present for every column and
       * that they are in the correct order. With the data coming from the
       * table definition in the FRX, this assumption should hold true as long
       * as the table is defined correctly.
       */

      /**
       * The column content should be everything in between the <th> or <td>.
       * This kind of makes the assumption that Forena has already provided
       * a consistent column format that will be a th or td. We want to pass
       * the whole column content including any tags along to the datatables
       * client. This is fairly safe because Forena will encode html in user
       * supplied data on a filed unless the field formatter specifically
       * allows xhtml. The attributes of the tr are preserved to that they can
       * be set as properties in the json output.
       */

      foreach ($rows as $row) {
        // Only include row 0 when there is no header (thead)
        if ($i > 0 || $hasHeader === FALSE) {
          $newRow = [];
          foreach ($row as $column) {
            /**
             * This is a poor way to remove the outer th and td tags but
             * simpleXMLElement can not handle this and converting every column to a
             * node to use DOM would be way too inefficient on tens of thousands of
             * rows. A ">" or "<" in an attribute will probably break the regex but
             * it seems unlikely.
             *
             * No real character escaping needs to happen because the json
             * encode should handle everything.
             */
            $value = preg_replace('/((<td[^>]*>)|(<\/td>)|(<th[^>]*>)|(<\/th>))/UD',
              "", $column->asXML());
            $value = decode_entities($value);
            $value = trim($value);

            /**
             * If the code is updated at some point to return an array of
             * objects instead of an array of arrays then extra attribute data
             * could be returned along with the column contents using the same
             * attribute array idea that is used for the headers when they are
             * read from the <thead>;
             */

            $newRow[] = $value;
          }
          $dataRows[] = $newRow;
        }
        $i++;
      }
    }
    return $dataRows;
  }
}

